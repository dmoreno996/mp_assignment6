﻿
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace MapsAssignment5
{
    public partial class MapsAssignment5Page : ContentPage
    {
        public MapsAssignment5Page()
        {
            InitializeComponent();
            var initialMapLocation = MapSpan.FromCenterAndRadius(new Position(33.1307785, -117.1601826), Distance.FromMiles(1));
            MyMap.MoveToRegion(initialMapLocation);
            PlacePins();
        }
        private void PlacePins()
        {
            var position = new Position(53.4631, -2.2913);
            var manchester = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "Old Trafford",
                Address = "My favorite team Manchester United FC"
            };
            MyMap.Pins.Add(manchester);

            position = new Position(40.4531, -3.6883);
            var madrid = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "Santiago Bernabeu",
                Address = "Current Reigning Champions League Winners Real Madrid"
            };
            MyMap.Pins.Add(madrid);

            position = new Position(51.4926, 7.4519);
            var dortmund = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "Westfalenstadion",
                Address = "Home of current American wonderkid Christian Pulisic, Borussia Dortmund"
            };
            MyMap.Pins.Add(dortmund);

            position = new Position(33.8644, -118.2611);
            var galaxy = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "StubHub Center",
                Address = "Attended USA mens soccer game as well as Manchester United friendly"
            };
            MyMap.Pins.Add(galaxy);

            position = new Position(55.7158 , 37.5537);
            var russia = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "Luzhniki Stadium",
                Address = "Wil host First and Last games of 2018 World Cup"
            };
            MyMap.Pins.Add(russia);
        }

        void Handle_ValueChanged(object sender, Xamarin.Forms.ValueChangedEventArgs e)
        {
            var zoomLevel = e.NewValue;
            var latlongdegrees = 360 / (System.Math.Pow(2, zoomLevel));
            MyMap.MoveToRegion(new MapSpan(MyMap.VisibleRegion.Center, latlongdegrees, latlongdegrees));
        }

        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            Picker pinPicker = (Picker)sender;
            int index = pinPicker.SelectedIndex;
            switch(index)
            {
                case 0:
                    MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(53.4631, -2.2913), Distance.FromMiles(1)));
                    break;
                case 1:
                    MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(40.4531, -3.6883), Distance.FromMiles(1)));
                    break;
                case 2:
                    MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(51.4926, 7.4519), Distance.FromMiles(1)));
                    break;
                case 3:
                    MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.8644, -118.2611), Distance.FromMiles(1)));
                    break;
                case 4:
                    MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(55.7158, 37.5537), Distance.FromMiles(1)));
                    break;
                default:
                    MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.1307785, -117.1601826), Distance.FromMiles(1)));
                    break;
            }
        }

        //change map style
        void Handle_Clicked(object sender, System.EventArgs e)
        {
            var button = sender as Button;
            string myValue = button.Text;
            if(myValue == "Hybrid")
            {
                MyMap.MapType = MapType.Hybrid; 
            }
            else if(myValue == "Satellite")
            {
                MyMap.MapType = MapType.Satellite;  
            }
            else if(myValue == "Street")
            {
                MyMap.MapType = MapType.Street; 
            }
        }
    }
}
